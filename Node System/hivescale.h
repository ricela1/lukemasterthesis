/*
*	Filename:	hivescale.h
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This file is the header file for hivescale.c. It includes port specifics, 
*				debugging settings, I2C settings and ADS1015 register defines, as well as
*				function prototypes. 
*
*/

#ifndef HIVESCALE_H
#define HIVESCALE_H

#define DEBUG 0x01

//Define I2C device address
#define PORT_ADDR 0x48

//PORT defines
#define I2C_PORT				PORTE
#define I2C_DDR					DDRE
#define	SDA						0x05
#define	SCL						0x04

//Load Cell Port
#define LOAD_CELL_DDR			DDRD
#define	LOAD_CELL_PORT			PORTD
#define	LOAD_CELL_PIN			0x02

//ADC specific bits
#define START_CONVERSION_BIT 	0x0F
#define PGA_BIT 				0x09
#define PGA_VALUE				0x03	//PGA=+-1.024V
#define	OPERATING_MODE_BIT		0x08	//Single-shot
#define DATA_RATE_BIT			0x05	
#define DATA_RATE_VALUE			0x00	//Lowest data rate

//Registers
#define CONV_REG	0x00
#define CONF_REG	0x01
#define LO_THRESH	0x02
#define HI_THRESH	0x03

//Ports
#define ALERT_PORT	PORTD
#define ALERT_IN	PIND
#define ALERT_DDR	DDRD
#define ALERT_PIN	0x00

//Load Cell Port
#define LOAD_CELL_DDR			DDRD
#define	LOAD_CELL_PORT			PORTD
#define	LOAD_CELL_PIN			0x02

//Address of ADC
#define ADDRESS 	0x90	

//function prototypes
int read_register(byte address,byte reg);
int get_adc_12bit();
int init_hivescale();
void printReading_hs();
void gather_data_hs();

#endif

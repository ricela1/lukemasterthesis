/*
*	Filename:	gsm_comms.h
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This is the header file for gsm_comms.c
*
*/

#ifndef GSM_COMMS_H
#define GSM_COMMS_H

//-----------------------
//Defines
//-----------------------
#define MIN_DELAY	20 		//minimum delay between commands
#define OK			1
#define ERROR		0

//Result codes from GSM Modem
#define RC_OK	0
#define RC_CONN	1
#define RC_ERR	4

#define DEBUG_GSM	0

//Determine whether to use GET or POST requests to send data
#define GET

//Number of times to retry a command if 
//an error code is returned
#define NUM_RETRIES	3


//Function prototypes
void init_comms_gsm(void);
byte sendCommand(char command[],char result[],byte timeout,byte numretries);
void gsm_date_time_server(unsigned long int * dt);
int start_gsm_transfer();

#endif

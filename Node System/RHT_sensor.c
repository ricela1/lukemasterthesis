/*
*	Filename:	RHT_sensor.c
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This file contains the driver for accessing the RHT03 temperature
*				humidity sensor using and interrupt driven 1 wire interface. 
*
*/

//Includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include "RHT_sensor.h"
#include "usart_comm_driver.h"
#include <avr/wdt.h> 
#include "kernel.h"

//RTC value for detecting hangup
extern int date_time_unix;

//Global buffers and transfer state
volatile byte timestamps[84];
volatile byte ths_state = 0;
volatile byte ths_bitcounter=0,ths_falling_edge=0;
volatile byte ths_error=0;

//buffered values for each sensor
volatile int temperatures[NUM_SENSORS];
volatile int humidities[NUM_SENSORS];

/*
*	Function Name:	init_sensor_ths()
*	Purpose:		Initialize individual sensor before reading. Function enables
*					pin change interrupt for given serial pin 
*/
byte init_sensor_ths(byte pin_number)
{
	//Reset recieve state variables
	ths_bitcounter=0;
	ths_error=0;

	//Install signal handler for a watchdog
	k_signal(&ths_sighandler,20);
	
	//Disable the USART recieve complete interrupt during the duration of the transfer
	//because another interrupt could cause a single bit to be missed. 
	UCSRB&= ~(1<<RXCIE);

	//Configure I/O ports. Must be on PORTB for the pin change interrupt to work
	DDRB|= (1<<pin_number);			//set to output

	//Handshaking protocol with RHT03
	PORTB|=(1<<pin_number);			//drive high
	_delay_us(100);
	PORTB&=~(1<<pin_number);  		//drive low
	_delay_ms(3);
	
	//Ensure global interrupts are enabled
	sei();
	
	//Configure I/O pin for transfer, input with pullups enabled
	DDRB&=~(1<<pin_number);		//set to input
	PORTB|=(1<<pin_number);		//with pullup

	//Enable PCINT<X>
	PCMSK1|=(1<<pin_number);

	//Clear TIMERCOUNTER1
	TCNT1H=0x00;
	TCNT1L=0x00;

	//Set TIMERCOUNTER1 prescaler to 1. This causes it to count on 2Mhz frequency or
	//once every half microsecond (.5us). 
	TCCR1B |= 0x01;

	//Enable global pin change interrupts
	EIMSK|=0x08;
	
	return 0x01;
}

/*
*	Function Name:	acquire_data_ths()
*	Purpose:		Acquire data from an individual RHT03 on a given pin
*/
byte acquire_data_ths(byte pin_number)
{
	//Initialize sensor 
	if(init_sensor_ths(pin_number)){

		//Busy-wait look while waiting for conversion to finish in the background. 
		//If the sensor locks up or a bit is missed, signal handler will set ths_error
		//which will disable the sensor. 
		while(ths_bitcounter<84 && !ths_error);

		//Unregister signal handler when read is complete. 
		k_unsignal();

		//disable PCINT8, 9 and 10
		PCMSK1&=~(0x07);

		//Disable and timer counter 1 and external interrupts
		TCCR1B&=~(0x07);
		EIMSK&=~(0x08);

		//Reset transfer state
		ths_bitcounter=0;
		ths_state = 0;
		
		//Re-enable UART recieve complete. 
		UCSRB|=(1<<RXCIE);

		//Post-process the datapoints gathered		
		if(!ths_error){
			return(process_data());
		}	
		else
		{
			ths_error=0;
			return 0;
		}
	}	
	return 0;
}

/*
*	Function Name:	gather_data_ths()
*	Purpose:		This function initiates acquisition from each RHT03 sensor connected
*					and stores the result in the global buffer for later printing during
*					transmission.
*/
void gather_data_ths()
{
		//Attempt to acquire temp and humid data from the sensor on pin PORTB0
		byte err=acquire_data_ths(0x00);

		//If an error occurs, retry 3 times at most. err is inverted from normal. 
		if(!err)
		{
			for(byte i=0;i<3;i++)
			{
				//attempt again to acquire data
				err=acquire_data_ths(0x00);

				//If no error, break out of loop
				if(err)
					break;	
			}
		}

		//If still there is an error (above for loop exited normally). 
		if(!err)
		{
			temperatures[0]=0xFFFF;
			humidities[0]=0xFFFF;
		}
		//No error, store data
		else
		{
			temperatures[0]=get_temp();
			humidities[0]=get_humidity();
		}



		//attempt to acquire temp and humid data from sensor at PORTB1
		err=acquire_data_ths(0x01);

		//if an error occurs, retry 3 times at most
		if(!err)
		{
			for(byte i=0;i<3;i++)
			{
				//attempt again to acquire data
				err=acquire_data_ths(0x01);

				//If no error, break out of loop
				if(err)
					break;	
			}
		}

		//If still there is an error (above for loop exited normally). 
		if(!err)
		{
			temperatures[1]=0xFFFF;
			humidities[1]=0xFFFF;
		}
		//No error, store data
		else
		{
			temperatures[1]=get_temp();
			humidities[1]=get_humidity();
		}

		//attempt to acquire temp and humid data from sensor at PORTB2
		err=acquire_data_ths(0x02);

		//if an error occurs, retry 3 times at most
		if(!err)
		{
			for(byte i=0;i<3;i++)
			{
				//attempt again to acquire data
				err=acquire_data_ths(0x02);

				//If no error, break out of loop
				if(err)
					break;	
			}
		}

		//If still there is an error (above for loop exited normally). 
		if(!err)
		{
			temperatures[2]=0xFFFF;
			humidities[2]=0xFFFF;
		}
		//No error, store data
		else
		{
			temperatures[2]=get_temp();
			humidities[2]=get_humidity();
		}
}

/*
*	Function Name:	printReading_ths()
*	Purpose:		This function prints out each sensors temperature and humidity
*					in CSV format. 
*/
void printReading_ths()
{	 
		//NEW CODE
		for(byte i=0;i<NUM_SENSORS;i++)
		{
			printDecimal(temperatures[i],16);
			sendChar(',');
			printDecimal(humidities[i],16);
			
			if(i!=NUM_SENSORS-1)
				sendChar(',');
		}	
}

/*
*	Function Name:	process_data()
*	Purpose:		This function post-processes the timestamps generated by the 
*					ISR to determine the state of recieved bits. 
*/
byte process_data()
{
	//Process the 84 rising and falling edges (42 total bits, the last 2 of which are not
	//data
	byte i,counter=0;
	for(i=4;i<84;i+=2)
	{
		//Low time signals a logical 0 bit
		if(timestamps[i]<0x45 && timestamps[i]>0x30)
		{
			timestamps[counter]=0x00;
		}
		//Higher time signals a logical 1
		else if(timestamps[i]>0x55)
		{
			timestamps[counter]=0x01;
		}
		//Otherwise some error must have occured. 
		else
			timestamps[counter]=0x02;

		counter++;
	}

	//return the checksum to let caller determine if the processing resulted in 
	//accurate readings. 
	return check_checksum();
}

/*
*	Function Name:	PCINT1_vect ISR
*	Purpose:		Interrupt service routine for detecting rising and falling edges.
*					Due to the fast timing of the sensor, the ISR has to be very short. 
*/
ISR(PCINT1_vect)
{
	timestamps[ths_bitcounter]=TCNT1;		
	ths_bitcounter++;
	TCNT1L=0x00;
}

/*
*	Function Name:	get_humidity()
*	Purpose:		This function extracts the 16 bit humidity reading out of the 40 bits
*					of data returned by the sensor
*/
int get_humidity(){
	int i;
	int H=0;
	for(i=0;i<16;i++){
		H=H<<1;
		H|=timestamps[i];
	}
	return H;
}

/*
*	Function Name:	get_temp()	
*	Purpose:		This function extracts the 16 bit temperature reading out of the 40 bits
*					of data returned by the sensor
*/
int get_temp(){
	int i;
	int T=0;
	for(i=16;i<32;i++){
		T=T<<1;
		T|=timestamps[i];
	}

	return T;
}

/*
*	Function Name:	get_checksum()
*	Purpose:		This function extracts the 8 checksum bits in the 40 bit reading from 
*					the sensor.
*/
byte get_checksum(){
	int i;
	int C=0;
	for(i=32;i<40;i++){
		C=C<<1;
		C|=timestamps[i];
	}

	return C&0xFF;
}

/*
*	Function Name:	check_checksum
*	Purpose:		This function checks the checksum against the rest of the sensor
*					readings. This is done by summing up the 4 bytes of temperature
*					and humidity readings and comparing it to the checksum. 
*/
byte check_checksum()
{
	byte b1,b2,b3,b4;
	int checksum;
	int humidity = get_humidity();
	int temp = get_temp();

	b1=(humidity>>8)&0x00FF;
	b2=humidity&0x00FF;
	b3=temp&0x00FF;
	b4=(temp>>8)&0x00FF;

	checksum=b1+b2+b3+b4;
	checksum = checksum & 0x00FF;

	return !(checksum^get_checksum());
}

/*
*	Function Name:	ths_sighandler
*	Purpose:		This function is fired by the signal interrupt in the kernel
*					and triggers the acquisition loop to exit in the case a read
*					from the sensor takes too long (around 2 seconds).
*/
void ths_sighandler(void)
{
	ths_error=1;
}

/*
*	Filename:	usart_comm_driver.h
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This is the header file for usart_comm_driver.c
*
*/

#ifndef USART_COMM_DRIVER_H
#define USART_COMM_DRIVER_H

//QCB status codes
#define QUEUE_EMPTY 0x00
#define QUEUE_FULL 0x01
#define QUEUE_NEOF 0x02

//Include brings byte definition
#include "kernel.h"

//QCB struct. QCB is a circular buffer. 
typedef struct {
	volatile byte in;		//In pointer
	volatile byte out;		//out pointer
	volatile byte* base;	//current position
	volatile byte status;	//Full, empty, etc. 
	volatile int size;		//Number of elements in queue. 
}QCB;

//Function prototypes. 
void USART_Comm_Init(void);
int USART_send(char* pmsg,byte len,byte memsource);
int USART_receive(char* pbuf,byte len);
int USART_receive_last_n(char* pbuf,byte len);
int USART_receive_nowait(char* pbuf,byte len);
void USART_RX_flush();
int write_q(QCB* pqcb,char data);
int read_q(QCB* pqcb,char* pdata);
void sendChar(char data);
void sendString(char s[]);
void sendString_P(char s[]);
char numberToChar(byte number);
char receiveCharacter();
void printDecimal(int itp,int base);
void newline();

#endif

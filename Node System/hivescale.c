/*
*	Filename:	hivescale.c
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This file contains the algorithm to acquire data from the Nasa Hivescale
*				built by Wayne Esaias. The scale is accessed through the ADS1015 12 bit
*				ADC that uses an I2C interface. The scale itself uses a load cell that 
*				must be 'excited' by a 5V signal which is sent from the microcontroller. 
*
*/

//This driver makes use of a I2C interface written by the instructable.com user "doctek" at 
//http://www.instructables.com/id/I2C_Bus_for_ATtiny_and_ATmega/. Rights to all code in
//USI_TWI_MASTER.c and .h are given to doctek and Instructables.com.

//Includes
#include <avr/io.h>
#include <util/delay.h>
#include "USI_TWI_Master.h"
#include "usart_comm_driver.h"
#include "hivescale.h"

//Union that allows reading and writing from the same structure
typedef union
{
	int int_value;
	byte bytes[4];
}I2C_PACKAGE;

//Globals
byte reg,temp;	
I2C_PACKAGE init_struct;
int data;

//function prototypes
int read_register(byte address,byte reg);

int get_adc_12bit();

/*
*	Function Name: 	printReading_hs()
*	Purpose:		Print a single previously gathered reading from the hivescale. 
*/
void printReading_hs(){
	printDecimal(data,16);
}

/*
*	Function Name: 	gather_data_hs()
*	Purpose:		gather a single reading from the hivescale as an average of 8 readings
*/
void gather_data_hs()
{
	//Initialize the ADS1015 ADC
	if(init_hivescale())
	{
		//Configure load cell excitation pin
		LOAD_CELL_DDR|=(1<<LOAD_CELL_PIN);
		data=0;

		//Excite load cell. Stays "excited" during whole 8 readings
		LOAD_CELL_PORT|=(1<<LOAD_CELL_PIN);
		_delay_ms(100);	//added 4/21/2013 after seeing weird results	
		
		//Average 8 readings with 30 ms delay between
		for(int i=0;i<8;i++) //added 4/21/2013 after seeing weird results
		{
			//gather data
			data+=get_adc_12bit();
			_delay_ms(30);
		}

		//de-excite load cell
		LOAD_CELL_PORT&=~(1<<LOAD_CELL_PIN);
		data=data>>0x03;
	}
	//Some error in contacting the ADS1015
	else{
		data = 0xFFFF;
	}
}

/*
*	Function Name: 	init_hivescale()
*	Purpose:		Initialize the ADS1015 ADC and configure communication ports. 
*/
int init_hivescale()
{
	#ifdef DEBUG 
		sendString("Starting Conversion\r\n");
	#endif

	//Initialize USI port for I2C communication
	USI_TWI_Master_Initialise();

	#ifdef DEBUG 
		sendString("initialization done\r\n");
	#endif

	//Configure alert port
	ALERT_DDR&=~(0x01<<ALERT_PIN);	//input
	ALERT_PORT&=~(0x01<<ALERT_PIN);	//disable pullup


	//Configure ADS1015 LO_thresh register to make alert pin signal conversion complete
	reg = LO_THRESH;		//LOW threshold register
	
	init_struct.bytes[0] = ADDRESS&0xFE;	//enable write
	init_struct.bytes[1] = reg;
	init_struct.bytes[2] = 0;
	init_struct.bytes[3] = 0;

	//Write command to I2C
	temp = USI_TWI_Start_Read_Write(init_struct.bytes,4);
	if(!temp)
	{
		#ifdef DEBUG 
			sendString("Setting LO_THRESH failed\r\n");
		#endif
	}
	

	//Configure ADS1015 HI_thresh register to make alert pin signal conversion complete
	reg = HI_THRESH;		//HI threshold register
	
	init_struct.bytes[0] = ADDRESS&0xFE;
	init_struct.bytes[1] = reg;
	init_struct.bytes[2] = 0x80;
	init_struct.bytes[3] = 0;

	//Write command to I2C
	temp = USI_TWI_Start_Read_Write(init_struct.bytes,4);
	if(!temp)
	{
		#ifdef DEBUG 
			sendString("Setting HI_THRESH failed\r\n");
		#endif
	}
}

/*
*	Function Name: 	get_adc_12bit()
*	Purpose:		Take single analog reading from ADS1015
*/
int get_adc_12bit()
{	
	//Build control-register 16 bit value. 
	int reg_value =	(1<<START_CONVERSION_BIT)|(PGA_VALUE<<PGA_BIT)|
	(1<<OPERATING_MODE_BIT)|(DATA_RATE_VALUE<<DATA_RATE_BIT);

	//Select register address
	reg=CONF_REG;

	//Build 4 byte transfer
	init_struct.bytes[0] = ADDRESS;
	init_struct.bytes[1] = reg;
	init_struct.bytes[2] =(reg_value>>0x08);
	init_struct.bytes[3] = reg_value;

	//Write command to ADS1015
	temp = USI_TWI_Start_Read_Write(init_struct.bytes,4);
	if(!temp)
	{
		#ifdef DEBUG sendString("Start conversion failed\r\n");
		#endif
	}

	//Read conversion register
	return read_register(ADDRESS,CONV_REG);
}

/*
*	Function Name: 	read_register()
*	Purpose:		Read a single 16 bit register from the ADS1015
*/
int read_register(byte address,byte reg)
{
	//Select I2C address (always the same, 0x48, as configured by the pins on the
	//actual ADS1015 board. See wiring schematic and datasheet). 
	init_struct.bytes[0]=ADDRESS&0xFE;

	//Set local register number
	init_struct.bytes[1]=reg;			//select conversion register

	//Select register to read from and start conversion. 
	temp = USI_TWI_Start_Read_Write(init_struct.bytes,2);
	if(!temp)
	{
		#ifdef DEBUG sendString("[READ] Select Register Failed\r\n");
		#endif
	}

	//Wait for alert signal to be pulled low signaling conversion complete
	while(ALERT_IN&(0x01<<ALERT_PIN));

	//Read from ADS1015
	init_struct.bytes[0]=ADDRESS|0x01;	//enable read bit
	temp = USI_TWI_Start_Read_Write(init_struct.bytes,4);

	//Check to see if read was successful. 
	if(!temp)
	{
		#ifdef DEBUG sendString("[READ] read failed\r\n");
		#endif
	}

	//Assemble multiple returned bytes
	int voltage = init_struct.bytes[1]<<4;
	voltage|=init_struct.bytes[2]>>4;

	return voltage;
}

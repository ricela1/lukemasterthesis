/*
*	Filename:	types.h
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This header file contains a few basic types that are used in many different
*				other files. 
*
*/

#ifndef TYPES_H
#define TYPES_H

typedef unsigned char byte;
typedef unsigned long int time_t;
#define TRUE	1
#define FALSE	0

#endif

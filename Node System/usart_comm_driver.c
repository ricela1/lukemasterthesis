/*
*	Filename:	usart_comm_driver.c
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This file contains the driver used for UART serial communication. 
*				There are various helper functions for both interrupt driven 
*				and non-interrupt driven UART communication, as well as some 
*				string processing and output functions and input and output queues. 
*
*/

#include "USART_Comm_driver.h"
#include <stdlib.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/atomic.h>
#include <math.h>

//Define queues. TX queue is empty because interrupt driven transmiting is not used
#define QUEUE_SIZE 64
volatile unsigned char tx_q[0];
volatile unsigned char rx_q[QUEUE_SIZE];

//Define queue control buffer structs. 
volatile QCB transmit_QCB = {0,0,&tx_q,QUEUE_EMPTY,QUEUE_SIZE};
volatile QCB receive_QCB = {0,0,&rx_q,QUEUE_EMPTY,QUEUE_SIZE};
volatile byte buffer=0x00;
//extern byte input_flag;

/*
*	Function Name:	USART0_RX_vect ISR
*	Purpose:		This ISR takes input from the UDR buffer (recieve register) and 
*					stores it into the recieve QCB. 
*/
ISR(USART0_RX_vect)
{
	//Take character from recieve buffer
	volatile byte test=0x00;
	test=UDR;

	//Put it into queue, as long as the queue isn't full
	if(receive_QCB.status!=QUEUE_FULL && test!=0x0A){
		write_q(&receive_QCB,test);
	}
}

/*
*	Function Name:	USART0_UDRE_vect ISR
*	Purpose:		This ISR takes data out of the transmit QCB whenever the transmit
*					buffer is empty (after a character gets transmitted successfully)
*					and transmits it. 
*/
ISR(USART0_UDRE_vect)
{	
	//If there isn't anything in the queue, disable the interrupt
	if(transmit_QCB.status==QUEUE_EMPTY)
	{
		UCSRB&=~(1<<UDRIE);
	}
	//Otherwise, read a byte from the transmit QCB and write it into the UDR. 
	else
	{
		read_q(&transmit_QCB,&UDR);
	}
}

/*
*	Function Name:	USART_send
*	Purpose:		This function sends a string out of the UART using interrupt-driven
*					transmit. Not really used, never worked without bugs. 
*/
int USART_send(char* pmsg,byte len,byte memsource)
{
	//Busy wait when the transmit queue is full, should probably ensure that the
	//interrupt is enabled here in case the buffer is full and the interrupt is
	//disabled
	while(transmit_QCB.status==QUEUE_FULL);

	//Write each byte to the queue. Should probably also check whether or not the queue
	//is full after each write, or check before hand to make sure it has room for all
	//of the bytes. 
	for(byte i=0;i<len;i++)
	{
		write_q(&transmit_QCB,pmsg[i]);		//write to tx_q
	}

	//Enable transmit ISR 
	UCSRB |= (1<<UDRIE);					//Enable UDRE interrupt - pg 161
	return len;
}

/*
*	Function Name:	USART_receive
*	Purpose:		This function is a blocking read from the UART, reading len number of 
*					bytes (characters) from the UART.
*/
int USART_receive(char* pbuf,byte len)
{
	//If the recieve queue is empty, wait until something is recieved. 
	while(receive_QCB.status==QUEUE_EMPTY);

	//Retrieve len bytes from the queue
	for(byte i=0;i<len;i++)
	{
		read_q(&receive_QCB,&pbuf[i]);
	}
	return len;
}

/*
*	Function Name:	USART_receive_nowait
*	Purpose:		This function is a non-blocking read from the UART, reading len 
*					number of bytes (characters) from the UART. If there are not len bytes
*					in the queue, function returns 0. 
*/
int USART_receive_nowait(char* pbuf,byte len)
{
	byte items_in_queue=0;

	//Determine number of items in the queue. 
	if(receive_QCB.in>receive_QCB.out){
		items_in_queue=receive_QCB.in-receive_QCB.out;
	}
	else if(receive_QCB.out>receive_QCB.in)
	{
		items_in_queue=receive_QCB.size-(receive_QCB.out-receive_QCB.in);
	}
	else
		return 0;


	//Check if there are len items in the queue. 
	if(items_in_queue>=len)
	{
		//Read len bytes from recieve buffer. 
		for(byte i=0;i<len;i++)
		{
			read_q(&receive_QCB,&pbuf[i]);	
		}
		return len;
	}
	else{
		return 0;
	}
}

/*
*	Function Name:	USART_receive_last_n
*	Purpose:		This function is blocking read that reads the most recent len items 
*					from the head of the recieve queue. This is used to ignore recieved
*					data and just get the most recent items. This avoids having to 
*					flush the buffer when useless data is recieved (such as an HTTP
*					response). 
*/
int USART_receive_last_n(char* pbuf,byte len)
{
	byte i,num_elements=0;

	while(receive_QCB.status==QUEUE_EMPTY);

	//Determine the number of (unread) items in the queue. 
	while(num_elements<len)
	{
		if(receive_QCB.in<receive_QCB.out)
		{
			num_elements=receive_QCB.size-(abs(receive_QCB.in-receive_QCB.out));
		}
		else
		{
			num_elements=receive_QCB.in-receive_QCB.out;
		}
	}
	
	char discard_buffer;
	
	//discard new bytes up to the bytes requested
	for(i=0;i<num_elements-len;i++)
	{
		read_q(&receive_QCB,&discard_buffer);
	}

	//Read len bytes from the end of the buffer
	for(i=0;i<len;i++)
	{
		read_q(&receive_QCB,&pbuf[i]);
	}
}

/*
*	Function Name:	USART_RX_flush
*	Purpose:		This function flushes the recieve buffer in the event that you want
*					to capture a new response to an output ignoring potential unread
*					bytes already in the buffer. USART_receive_last_n is another method
*					to do this. 
*/
void USART_RX_flush()
{
	receive_QCB.in=0;
	receive_QCB.out=0;
	receive_QCB.status=QUEUE_EMPTY;
}

/*
*	Function Name:	write_q
*	Purpose:		This function is used to write to a QCB (queue control block). It is
*					either called by an ISR recieving a character or by user code to insert
*					a new character into transmit QCB. 
*/
int write_q(QCB* pqcb,char data)
{
    //check to see if queue is full
    while((*pqcb).status==QUEUE_FULL);

	//This ensures that the queue isn't modified by ISR's outside of this function
	ATOMIC_BLOCK(ATOMIC_FORCEON)
	{
	        //write data to end of queue
	        (*pqcb).base[(*pqcb).in]=data;

	        //Check to see if end of queue has been reached
	        if((*pqcb).in<(*pqcb).size-1)
	        {
	            //increment
	            (*pqcb).in++;
	        }
	        else
	        {
	            //wrap around
	            (*pqcb).in=0;
	        }

	        //set status flag
	       if((*pqcb).in==(*pqcb).out){
	            (*pqcb).status=QUEUE_FULL;
			}
	        else{
	            (*pqcb).status=QUEUE_NEOF;
			}
	}

    return 0;
}

/*
*	Function Name:	read_q
*	Purpose:		This function is used to read from a QCB (queue control block). It is
*					either called by an ISR transmitting from the transmit QCB or by user
*					code reading unread bytes from the recieve QCB. 
*/
int read_q(QCB* pqcb,char* pdata)
{
	//wait for queue to become non-empty
	while((*pqcb).status==QUEUE_EMPTY);

	//This ensures that the queue isn't modified by ISR's outside of this function
    ATOMIC_BLOCK(ATOMIC_FORCEON)
    {
	        //Read byte from queue
	        *pdata=(*pqcb).base[(*pqcb).out];

		    //Check to see if end of queue has been reached
		    if((*pqcb).out<(*pqcb).size-1)
		    {
		        //increment
		        (*pqcb).out++;
		    }
		    else
		    {
		        //wrap around
		        (*pqcb).out=0;
		    }

	        //update status
	        if((*pqcb).in==(*pqcb).out){
	            (*pqcb).status=QUEUE_EMPTY;
			}
	        else
	            (*pqcb).status=QUEUE_NEOF;
	}
	return 0;
}

/*
*	Function Name:	USART_Comm_Init
*	Purpose:		This function initializes the UART
*/
void USART_Comm_Init(void)
{
	/* Set baud rate to 19200 for U2X async mode @ 2Mhz clock - page 175*/
	UBRRH = 0;
	UBRRL = 12;

	/* Enable U2X  - pg 171 in datasheet*/
	UCSRA |= (1<<U2X);

	/* Enable receiver and transmitter */
	UCSRB = (1<<RXEN)|(1<<TXEN);

	/* Enable Asynchronous communication */
	UCSRC = (0<<UMSEL);

	/* Parity
	   0 0 = no parity
	   0 1 = reserved
	   1 0 = even parity
	   1 1 = odd parity 
	*/
	UCSRC |= (0x00<<UPM0);

	/*Stop bit selection
	  USBS 0 = 1 stop bit
	  USBS 1 = 2 stop bits
	*/
	UCSRC |= (0<<USBS);

	/* Data size - page 174 of datasheet */
	UCSRC |= (0x03<<UCSZ0);

	/*Disable Clock polarity for async. Set 1 sync mode */
	UCSRC |= (0<<UCPOL);	

	/*Enable USART Interrupts*/
	UCSRB |= (1<<RXCIE);	//Receive Complete Interrupt Enable
	//UCSRB |= (1<<UDRIE);	//USART Data register empty IE

	/*Enable global Interrupts*/
	sei();
}

/*
*	Function Name:	sendChar
*	Purpose:		This function is used to manually send a single character from the 
*					UART using non-interrupt driven transmission. 
*/
void sendChar(char data)
{
    // To send data with the USART put the data in the USART data register 
    UDR = data;

	// Wait until the byte is sent
   	while( !(UCSRA&0x40) );
        
    // Clear the TXC (transmit complete) flag.         
	UCSRA=UCSRA|0x40;          
}

/*
*	Function Name:	sendString
*	Purpose:		This function is used to manually send an entire string from the 
*					UART using non-interrupt transfer. 
*/
void sendString(char s[])
{
	int i = 0;
	
	while(i < 64) //Don't end up transmitting all of memory on an error
	{
		if( s[i] == '\0' ) break; // quit on null terminator
		sendChar(s[i++]);
	}
}

/*
*	Function Name:	sendString_P
*	Purpose:		This function is used to manually send an entire string from  
*					program memory (PROGMEM) using the UART using non-interrupt transfer. 
*/
void sendString_P(char s[])
{
	int i = 0;
	
	while(i < 128) //Don't end up transmitting all of memory on an error
	{
		char buf = (PGM_P)pgm_read_byte(&(s[i++]));
		if( buf == '\0' ) break; // quit on string terminator
		sendChar(buf);
	}
}

/*
*	Function Name:	numberToChar
*	Purpose:		This function is a helper function that converts integer numbers
*					to their ASCII equivilants.  
*/
char numberToChar(byte number)
{
	if(number<0xA)
		return (char)number+0x30;
	else if(number<=0xF)
		return (char)number+0x37;
	else
		return (char)number;
}

/*
*	Function Name:	receiveCharacter
*	Purpose:		This function recieves a single character from the UART without 
*					using interrupts. Blocks on an empty recieve buffer locking 
*					the system unless a character is recieved. 
*/
char receiveCharacter()
{
	//Wait for data to be received
	while (!(UCSRA &(1<<RXC)));
	//return recieved character
	return UDR;
}

/*
*	Function Name:	newline
*	Purpose:		This function prints a full newline (CRLF)
*/
void newline()
{
	sendChar(0x0D);
	sendChar(0x0A);
}

/*
*	Function Name:	printDecimal
*	Purpose:		This function prints a 16 bit integer in whatever base is 
*					desired. 
*/
void printDecimal(int itp,int base)
{
	char decimal[6];
	itoa(itp,decimal,base);
	sendString(decimal);
}

/*
*	Function Name:	printByte
*	Purpose:		This function prints a single byte as ASCII
*/
void printByte(byte btp)
{
	char lob = numberToChar(btp & 0x0F);
	char hob = numberToChar((btp & 0xF0)>>0x04);
	sendChar(hob);
	sendChar(lob);
}

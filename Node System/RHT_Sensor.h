/*
*	Filename:	RHT_Sensor.h
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This is the header file for RHT_sensor.c
*
*/

#ifndef RHT_SENSOR_H
#define RHT_SENSOR_H

#include "types.h"

typedef struct{
	int temp;
	int humidity;
	byte checksum;
}rht_t;

//Defines
#define DP_MAX 			42		//maximum datapoints to detect
#define RHT_PIN 		0x1		
#define RHT_IN			PINB	//Input Port
#define RHT_OUT			PORTB	//Output Port
#define RHT_DDR			DDRB	//DDR Port
#define FALLING_EDGE	0x00
#define RISING_EDGE		0x01
#define NUM_SENSORS 	0x3		//Number of RHT03 sensors used

//Function Prototypes
byte acquire_data_ths(byte pin_number);
byte init_sensor_ths(byte pin_number);
void gather_data_ths();
void printReading_ths();
byte process_data();
int get_humidity();
int get_temp();
byte get_checksum();
byte check_checksum();
void ths_sighandler(void);

//Macros
#define lo8(x) ((x)&0xff) 
#define hi8(x) ((x)>>8)

#endif

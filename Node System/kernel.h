/*
*	Filename:	kernel.h
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This is the header file for kernel_v6.c
*
*/

#ifndef KERNEL_H
#define KERNEL_H

//Function prototypes make use of the byte typedef
#include "types.h"

//Function Prototypes
void k_sleep(int seconds);
void k_init_rtc(void);
void k_signal(void (*function)(void), byte mseconds);
void k_unsignal();
int gather_battery_voltage();
void gather_internal_temp();
void gather_light_sensor();
void printUptime();

#endif


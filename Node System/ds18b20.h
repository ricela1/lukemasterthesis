/*
*	Filename:	ds18b20.h
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This is the header file for ds18b20.c
*
*/

#ifndef DS18B20_H
#define DS18B20_H

//GPIO pin defines
#define W1_PIN	0x02
#define W1_IN	PIND
#define W1_OUT	PORTD
#define W1_DDR	DDRD

//Command code defines
#define READROM 	0x33
#define MATCHROM 	0x55
#define SKIPROM		0xCC
#define CONVERTT	0x44
#define READSCRATCH 0xBE

//Function Prototypes
void w1_sendByte(byte command);
void w1_write1();
void w1_write0();
byte w1_read(byte num_bytes,volatile byte* buffer);
void initSensor();
void printRomCode(byte* rom_code);
void matchRom(byte* rom_code);
int printTemperature(byte* rom_code);
void printReading();
void searchRomCode();
void ds_sighandler();
void init();

#endif

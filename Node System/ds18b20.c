/*
*	Filename:	ds18b20.c
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This file is the driver for using the DS18B20 temperature sensor. It 
*				contains the code for accessing the DS18B20 by a 1 wire interface using an
*				interrupt driven serial protocol. This file also contains buffers for 
*				storing readings for the 4 sensors used and their hard-coded 
*				ROM codes. 
*
*/

//Includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay.h>
#include <stdlib.h>
#include <avr/pgmspace.h>
#include <stdint.h>
#include "USART_Comm_Driver.h"
#include <stdio.h>
#include "ds18b20.h"

//Union storing ROM code in string and 64 bit integer
typedef union rom_code{
	long long int rom_code_int;
	byte rom_code_byte_array[8];
}ROM_CODE;

//Struct for storing ROM code and associated temperature value
typedef struct ds18b20{
	int temp;
	ROM_CODE rom_code; 
}DS18B20;

//Interrupt accessed buffers
volatile byte bitcounter=0;
volatile byte bytecounter=0;
volatile byte edgetracker=0;	//0 for falling, 1 for rising.
volatile int fallingedge=0;
volatile byte* bufptr;
volatile byte ds_error=0;

long long int rom_code=0;

//Global array of sensors
DS18B20 sensors[4];

/*
*	Function Name: 	init()
*	Purpose: 		initialize struct of sensors with associated ROM codes
*					sensor numbering is arbitrary 
*/
void init()
{
	//Assign ROM codes to sensors. These codes were determined
	//one at a time using code reading function. 
	sensors[0].rom_code.rom_code_int = 0xEC00000349A49B28;
	sensors[1].rom_code.rom_code_int = 0x3000000349A7B428;
	sensors[2].rom_code.rom_code_int = 0xC80000034999A328;
	sensors[3].rom_code.rom_code_int = 0xE10000034996C428;
}

/*
*	Function Name: 	w1_sendByte(byte channel)
*	Purpose: 		Send a single byte over the 1 wire interface.  
*/
void w1_sendByte(byte command)
{
	//Set GPIO port to output
	W1_DDR|=W1_PIN;

	//Transmit each bit
	byte i;
	for(i=0;i<8;i++)
	{
		//Check each bit and send either a 1 or a 0
		if((0x01<<i)&command)
			w1_write1();
		else
			w1_write0();
	}

	//Set port to input
	W1_DDR&=~(W1_PIN);

	//Enable pullup after transmission
	W1_OUT|=W1_PIN;
}

/*
*	Function Name: 	w1_write1()
*	Purpose: 		Send binary 1 over the 1 wire interface
*/
void w1_write1()
{
	//write 1
	W1_OUT&=~(W1_PIN); 	//Drive low
	_delay_us(5);		//Delay short indicates a 1
	W1_OUT|=W1_PIN;		//Drive high
	_delay_us(55);		//Keep high for remainder of period
}

void w1_write0()
{
	W1_OUT&=~(W1_PIN);	//Drive low
	_delay_us(60);		//Hold low for 60 us indicating a 0
	W1_OUT|=W1_PIN;		//Drive high
	_delay_us(2);		//Hold high briefly. 
}

/*
*	Function Name: 	w1_read1()
*	Purpose: 		read a series of bytes from the 1 wire interface
*/
byte w1_read(byte num_bytes,volatile byte* buffer)
{
	//Assign global to point to user buffer
	bufptr=buffer;

	ds_error=0;

	byte i,val=num_bytes*8;

	//Configure external interrupt
	EIMSK |= 0x01; 		//external int0 (DP1)
	EICRA |= 0x03;		//detect rising edges

	//Ensure global interrupts are enabled. 
	sei();

	//Interrupt housekeeping variables
	bitcounter=0;
	edgetracker=0;
	bytecounter=0;

	//Clear timer-counter 1 high and low bytes
	TCNT1H=0x00;
	TCNT1L=0x00;

	//init 16 bit timer with no divider
	TCCR1B |= 0x01;

	//Handshaking to trigger the DS18B20 to respond. 
	for(i=0;i<val;i++)
	{	
		W1_DDR|=W1_PIN;	 	//Set to output
		W1_OUT&=~(W1_PIN);	//Drive Low
		fallingedge=TCNT1;
		_delay_us(6);		//Delay
		W1_DDR&=~(W1_PIN);	//Set to input
		W1_OUT|=W1_PIN;		//Enable pullup
		_delay_us(55);		//Delay
	}

	//Wait for bits to be gathered. This won't lock up, even if the
	//DS18B20 doesn't respond, it is clocked by the MCU so this loop will
	//eventually break out regardless of if the DS18B20 responds. Interrupt 
	//handler will be gathering data during this empty loop. 
	while(bytecounter<num_bytes);
	
	//Disable external interrupt and reset resources after data is
	//transferred
	EIMSK&=~(0x01);
	TCCR1B&=0xF8;
	bitcounter=0;
	edgetracker=0;
	bytecounter=0;

	if(ds_error)
	{
		ds_error=0;
		return 0;
	}
	else
		return 1;
}

/*
*	Function Name: 	initSensor()
*	Purpose: 		Initialize DS18B20, this must be done between reads starting
*					conversions and between conversion and a read. 
*/
void initSensor()
{
	//Set as output and drive high for 50us
	W1_DDR|=W1_PIN;
	W1_OUT|=W1_PIN;
	_delay_us(50);

	//Drive low for >480 us
	W1_OUT&=~(W1_PIN);
	_delay_us(600);

	//Set to input and enable pullup
	W1_DDR&=~(W1_PIN);
	W1_OUT|=W1_PIN;

	//Delay before a read or write. 
	_delay_us(500);	
}

/*
*	Function Name: 	printRomCode(byte* rom_code)
*	Purpose: 		This function is used to format and output the 64 bit 
*					ROM code. 
*/
void printRomCode(byte* rom_code)
{
	printByte(rom_code[7]);
	printByte(rom_code[6]);
	printByte(rom_code[5]);
	printByte(rom_code[4]);
	printByte(rom_code[3]);
	printByte(rom_code[2]);
	printByte(rom_code[1]);
	printByte(rom_code[0]);	
}

/*
*	Function Name: 	searchRomCode()
*	Purpose: 		This function is used to interrogate a DS18B20 sensor and
*					determine its ROM code. ONLY 1 sensor can be connected 
*					during this process. 
*/
void searchRomCode()
{
	//Interrogate sensor
	byte rom_code[8];
	initSensor();
	w1_sendByte(READROM);
	w1_read(8,rom_code);

	//Print ROM Code
	printByte(rom_code[7]);
	printByte(rom_code[6]);
	printByte(rom_code[5]);
	printByte(rom_code[4]);
	printByte(rom_code[3]);
	printByte(rom_code[2]);
	printByte(rom_code[1]);
	printByte(rom_code[0]);	
}

/*
*	Function Name: 	matchRom(byte* rom_code)
*	Purpose: 		This function matches the ROM code stored in the sensor struct.
*					This triggers that sensor to listen to the following command,
*					such as CONVERT_T, otherwise no sensors will respond. 
*/
void matchRom(byte* rom_code)
{
	//Alert all sensors that the following 8 bytes will contain a ROM code. 
	w1_sendByte(MATCHROM);

	//Transmit ROM code
	w1_sendByte(rom_code[0]);
	w1_sendByte(rom_code[1]);
	w1_sendByte(rom_code[2]);
	w1_sendByte(rom_code[3]);
	w1_sendByte(rom_code[4]);
	w1_sendByte(rom_code[5]);
	w1_sendByte(rom_code[6]);
	w1_sendByte(rom_code[7]);
}

/*
*	Function Name: 	printTemperature(byte* rom_code)
*	Purpose: 		This function handles the entire temperature reading process
*					for a single DS18B20. 
*/
int printTemperature(byte* rom_code)
{
	//Declare scratchpad and the HOB and LOB of the resulting temperature reading
	byte scratchPad[9],hob,lob;

	//Initialize sensor, match the ROM code and issue conversion command
	initSensor();	
	matchRom(rom_code);
	w1_sendByte(CONVERTT);

	//brief delay to allow conversion to complete. Probably way too long
	_delay_ms(100);
	
	//Init sensor, match ROM and inform sensor that I want to read the 
	//scratchpad, where the temperature is stored. 
	initSensor();
	matchRom(rom_code);
	w1_sendByte(READSCRATCH);

	//Read temperature from the scratchpad. 
	if(w1_read(2,scratchPad)){

		//Store temperature locally.
		lob=scratchPad[0];
		hob=scratchPad[1];

		//Create 16 bit number out of bytewise temperature
		int temp;
		temp |= (hob<<8);
		temp |= lob;
		return temp;

	}
	else
	{
		//Error, return error value
		return 0xFFFF;
	}
}

/*
*	Function Name: 	printReading_ds()
*	Purpose: 		Print the temperature reading from each sensor. Does not acquire
*					the actual data though.  
*/
void printReading_ds()
{

	init();
	
	//CSV method. Allows each DS18B20 reading to be seperated later on the serverside. 
	byte i;
	for(i=0;i<4;i++)
	{
		printDecimal(sensors[i].temp,16);
		if(i<3){
			sendChar(',');
		}
	}
}

/*
*	Function Name: 	gather_data_ds()
*	Purpose: 		This function triggers the acquisition of data from each sensor
*					and stores it for future access when a transmission is about to 
*					be made.   
*/
void gather_data_ds()
{
	init();
	
	//Gather data from each sensor. 
	byte i;
	for(i=0;i<4;i++)
	{
		sensors[i].temp=printTemperature(sensors[i].rom_code.rom_code_byte_array);
	}
}

/*
*	Function Name: 	Interrupt service routine
*	Purpose: 		This ISR detects the changes in logic level on the 1 wire interface
*					pin and calculates the difference in high and low times which
*					signal whether the bit is a 1 or 0. The ISR builds the resulting
*					byte in real time. 
*/
ISR(INT0_vect)
{
	//This delay represents the amount of time between the starting of the timer and
	//detection of a falling edge. This number is changed by the length of the cable
	//(due to capacitance and resistance in the wire creating a filter) so the number
	//has to be adjusted depending on whether a long wire is used. A better way would
	//involve buffering all the samples and finding out the min max and average and 
	//calculating the low/high time from that. 

	//Detect a 1 bit
	if(TCNT1-fallingedge<110)	//ON FRAME AND LONG CABLE
	//if(TCNT1-fallingedge<70) 	//ON BREADBOARD
	{
		bufptr[bytecounter]|=(1<<bitcounter);
	}
	//0 bit. 
	else
		bufptr[bytecounter]&=~(1<<bitcounter);

	//track number of bits seen. 
	bitcounter++;

	//Reset bitcounter when a whole byte has been read and increment bytecounter 
	//so the next bits write to the next byte in the stream. 
	if(bitcounter>7){
		bytecounter++;
		bitcounter=0;
	}
}

/*
*	Filename:	gsm_comms.c
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This file is the driver for the Telit GE865 GSM modem. Most of the code
*				is independent of the GE865 and only 1 AT command is actually used. 
*				The rest of the AT commands are stored in an onboard configuration
*				that does not have to be sent every time the system runs. 
*
*/

//Includes
#include <avr/io.h>
#include <stdlib.h>
#include <stdio.h>
#include "kernel.h"
#include "gsm_comms.h"
#include <util/delay.h>
#include "USART_comm_driver.h"
#include "types.h"
#include <stdint.h>
#include <string.h>

extern time_t date_time_unix;

//Transmit and recieve buffers
extern QCB transmit_QCB;
extern QCB receive_QCB;

//Return code buffer from modem
volatile char rcbuf[20];

/*
*	Function Name: 	init_comms_gsm()
*	Purpose: 		This function initializes the GSM modem for communication. 
*					Take the process with a grain of salt, the GSM modem stores the 
*					configuration and doesn't require re-configuring on startup so
*					this function has basically never had to run.    
*/
void init_comms_gsm()
{
	/*
		*Initial Commands - un-needed usually as modem stores configuration
		*#SGACT (activate socket, gives you your IP address for the duration of socket) 
		*and modem is configured to automatically
		*do #SGACT on startup. 
	*/
	
	/*sendCommand(C_AT,"1\r",2,0);		//Ensures communication is active. Returns 0, does nothing
	sendCommand(C_SINT,"1\r",2,0);		//Makes sure you are using most recent (and suggested) command set
	_delay_ms(2000);
	sendCommand(C_CGDCONT,"1\r",2,0);	//Set up GPRS context. Select PDP (protocol, PPP or IP) and APN and 
										//static IP (since we don't have one, 0.0.0.0)
	sendCommand(C_SCFG,"1\r",2,0);		//Configure timeouts etc for a given connection
	sendCommand(C_SGACT,"1\r",2,0);		//Activate connection/socket. Doesn't open socket, just gets IP address
										//for the duration of the connection. You can make lots of GET's and POST's 
										//etc and only have to do AT#SGACT=X,1 once. Modem is configured to automatically
										//do it on modem startup, and all the other prior commands are stored in the
										//configuration profile. */

	//Disable command echo back to the MCU, only good to see what you type into
	//the terminal manually. 
	//sendCommand("ATE0\r\n","1\r",2,0);
}

/*
*	Function Name: 	start_gsm_transfer()
*	Purpose: 		This function initiates a GPRS transfer to the student server
*					on port 80. After the function returns, the modem is relaying
*					serial input over the GPRS interface. Current system just sends 
*					a CSV string, no HTTP methods.   
*/
int start_gsm_transfer()
{
	//send AT command to open the socket
	if(sendCommand("AT#SD=1,0,15004,student.cs.appstate.edu\r\n","1\r",10,0)){
		return 1;
	}
	else
		return 0;
}

/*
*	Function Name: 	sendCommand()
*	Purpose: 		This function sends an AT command and listens for a given response
*					code, timing out after a set number of seconds, retrying a specified
*					number of times on failure.  
*/
byte sendCommand(char command[],char result[],byte timeout,byte numretries)
{
	int numchars = strlen(result);
	byte i,found=0;
	time_t maxtime;

	//Send command until response is returned	
	for(i=1;i<=numretries+1;i++)
	{
		//Flush input buffer in case there is garbage in there. 
		USART_RX_flush();

		//Send AT command
		sendString(command);

		//Set timeout
		maxtime=date_time_unix+timeout;
	
		//Wait for response until timeout value is met. 
		while(date_time_unix<maxtime)
		{
			//Non-blocking input in case response code nver comes. 
			if(USART_receive_nowait(rcbuf,numchars))
			{
				found=1;
				break;
			}
			_delay_ms(500);
		}

		//Check to see if the response code came in
		if(found)
			break;
	}
	
	//Check recieved code
	if(found)
	{
		byte j,error=0;
		
		//Compare code recieved with the expected response code
		for(j=0;j<numchars;j++)
		{
			if(rcbuf[j]!=result[j])
			{		
				error=1;
				break;
			}
		}
		if(error){
			return 0;
		}
		else{
			return 1;
		}
	}	
	else
	{
		return 0;
	}

}

/*
*	Function Name: 	gsm_date_time_server()
*	Purpose: 		This is a deprecated function that was used to retrieve a 
*					UNIX timestamp from a server program that could be then
*					incremented by the RTC. I realized this was not useful but 
*					it did work for letting the system know the time. 
*/
void gsm_date_time_server(unsigned long int* dt)
{
	char tbuf[11];
	USART_RX_flush();

	//Open connection to timeserver, this triggered the timeserver to
	//transmit a timestamp. 
	sendCommand("AT#SD=1,0,15003,student.cs.appstate.edu\r\n","1\r",5,2);

	//_delay_ms(1000);	
	USART_receive(tbuf,2);	//catch and toss out connect indication

	//Read  digits of time
	USART_receive(tbuf,10);

	
	tbuf[9]='R'; //sentinel values - clobber "NO CARRIER" (0x33 0x0D) code 
	tbuf[8]='R';

	//Convert date/time hex from server to a long int
	*dt=strtol(tbuf,(char**)NULL,16);
}


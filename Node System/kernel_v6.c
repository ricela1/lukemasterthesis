/*
*	Filename:	kernel_v6.c
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This file contains the main kernel of the RHMS. It contains the main()
*				that is called when the program starts. This file contains the code that
*				initializes hardware resources, the state machine that gathers and
*				transmits data, and various helper functions to acquire data from analog
*				sensors and make use of software signals. 
*
*/

//Includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdint.h>
#include <avr/power.h>
#include "kernel.h"
#include "USART_Comm_Driver.h"
#include "RHT_Sensor.h"
#include "hivescale.h"
#include <stdio.h>
#include "gsm_comms.h"
#include "ds18b20.h"
#include "adc.h"
#include "USI_TWI_Master.h"

//Port definitions that allow source code to allow for easy modification of
//I/O pin usage. 

//LED output
#define STATUS_LED_PORT		PORTD
#define STATUS_LED_DDR		DDRD
#define STATUS_LED_PIN		0x00

//XBEE sleep port info
#define XBEE_SLEEP_PORT		PORTB
#define XBEE_DDR			DDRB
#define XBEE_PIN			0x05

#define RESET_DETECT_PORT	PORTD
#define RESET_DETECT_DDR	DDRD
#define RESET_DETECT_PIN	0x03
#define RESET_DETECT_IN		PIND

#define LDR_PORT			PORTD
#define LDR_DDR				DDRD
#define	LDR_PIN				0x04

//----------------------------------
//Date/Time info
//----------------------------------
volatile time_t date_time_unix=0x00000000;

//Signal counter variables
volatile byte signalMseconds=0;
volatile byte subCounter = 0;
volatile byte signalCounter=0;
volatile void (*signalFunction)(void)=NULL;
volatile byte toggle=0;

//uptime counter
unsigned int uptime=0;

//ADC values
int internal_temp,light_sensor,battery_voltage;

//connection error count
byte connection_error;

/*
*	Function Name:	main
*	Purpose:		Main driver function of entire program. This function is called on
*					startup of the system. This function is responsible for initializing
*					hardware components and the main state-machine of the whole RHMS system
*/
int main(void)
{	
	//DISABLE JTAG - Must be done at least twice to take
	MCUCR|=(1<<JTD);
	MCUCR|=(1<<JTD);
	MCUCR|=(1<<JTD);
	MCUCR|=(1<<JTD);

	//Configure XBEE port
	XBEE_DDR|=(1<<XBEE_PIN);

	//Configure RESET detect port
	RESET_DETECT_DDR&=~(1<<RESET_DETECT_PIN);	//input
	RESET_DETECT_PORT|=(1<<RESET_DETECT_PIN);	//with pullup

	//initialize USART and enable threading
	USART_Comm_Init();

	//Prevent error from double-triggering of butterfly with joystick
	_delay_ms(500);

	//Initialize real time clock and ADC
	date_time_unix=0x00;
	k_init_rtc();
	k_init_adc();

	//Put XBEE to sleep by asserting XBEE sleep pin (pin 9);
	XBEE_SLEEP_PORT &= ~(1<<XBEE_PIN);

	//Acquire data to test sensors
	gather_data_ths();
	gather_data_ds();
	gather_data_hs();
	gather_internal_temp();
	gather_battery_voltage();
	gather_light_sensor();

	while(1)
	{
		//Trigger each sensor to acquire data and buffer a single sample
		gather_data_ds();
		gather_data_ths();
		gather_data_hs();
		gather_internal_temp();
		gather_battery_voltage();
		gather_light_sensor();
		
		/****Transmit data****/

		//Ensure XBEE is awake by de-asserting sleep pin on XBEE (pin 9)
		XBEE_SLEEP_PORT |= (1<<XBEE_PIN);
		_delay_ms(40);	//min wakeup time for XBEE is 16ms

		//Attempt to open TCP connection over GSM modem
		if(start_gsm_transfer())
		{
			//Reset connection-error counter. 
			connection_error=0;

			//Write CSV style data to the UART which gets transmitted directly to the web
			//over already open connection. 
			sendString("0.0.");
			printUptime();
			sendChar(',');
			printReading_ths();
			sendChar(',');
			printReading_hs();
			sendChar(',');
			printDecimal(internal_temp,16);
			sendChar(',');
			printDecimal(battery_voltage,16);
			sendChar(',');
			printDecimal(light_sensor,16);
			sendChar(',');
			printReading_ds();
			sendString("\r\n");		//Triggers closing of the socket on the server-side
			
			//Detect reset request pin. Puts MCU in an infinite loop which allows
			//the operator to wirelessly toggle the reset pin which must be timed properly
			//to be detected. 
			if(!(RESET_DETECT_IN&(0x01<<RESET_DETECT_PIN)))
			{
				sendString("Reset request detected, staying awake to detect reset\r\n");

				//Infinite loop gives user time to send reset pulse. 
				while(1){
					sendString("a\r\n");
					_delay_ms(100);
				}
			}

			//Put XBEE to sleep by asserting XBEE sleep pin (pin 9);
			XBEE_SLEEP_PORT &= ~(1<<XBEE_PIN);
		
			//sleep for 10 minutes
			uptime++;
			k_sleep(600);
		}

		//Error in opening socket to web-server, wait 60 seconds and try again
		else
		{
			//Increment error counter. If have receieved 3 errors in a row
			//IE 3 failed to open sockets in a row, sleep for a minute and
			//try again (should reduce power used by errors)
			connection_error++;
			if(connection_error<3){
				
				//Put XBEE to sleep by asserting XBEE sleep pin (pin 9);
				XBEE_SLEEP_PORT &= ~(1<<XBEE_PIN);
				uptime++;
				k_sleep(60);
			}

			//Sleep for a long time, some major error must have happened that is
			//preventing transfers from taking place. 
			else
			{
				//Put XBEE to sleep by asserting XBEE sleep pin (pin 9);
			  	XBEE_SLEEP_PORT &= ~(1<<XBEE_PIN);
				
				//sleep for 10 minutes
				uptime++;
				k_sleep(600);
			}
		}
	}

}

/*
*	Function Name:	k_sleep
*	Purpose:		This function controls the sleep state of the system. The only
*					power save method used is SLEEP_MODE_PWR_SAVE but varying times
*					are required. The RTC must be enabled to allow sleeping for specific
*					times. 
*/
void k_sleep(int seconds)
{
	//Power saving - delay for 1 second and wake up to register real time clock
	//events and go back to sleep
	time_t sleepTime = date_time_unix+seconds;

	//Set sleep mode
	set_sleep_mode(SLEEP_MODE_PWR_SAVE);

	//Sleep for a single second. System is woken by RTC to keep track of
	//elapsed time. The watchdog timer can be used for longer sleeps but requires
	//removing the bootloader and resets whole system. 
	while(date_time_unix<sleepTime){

		//Enable sleep (SE bit)
		cli();
		sleep_enable();
		sei();

		//Put MCU to sleep
		sleep_cpu(); //sleep_cpu and sleep_mode seem to be equivilant, not sure the diff

		//Delay HAS to be here, otherwise for some reason, it will
		//register X seconds but it actually runs <=X/2 seconds
		//SEE IF THIS CAN BE MADE SMALLER, 10MS IS A LONG TIME!!
		_delay_ms(10);		

		sleep_disable();
	}
}

/*
*	Function Name:	k_init_rtc
*	Purpose:		This function initializes the real-time-clock. 
*/
void k_init_rtc(void)
{
	//Enable RTC as clock source
	ASSR=(1<<AS2);

	//Enable interrupt on overflow
	TIMSK2=(1<<TOIE2);

	//Set prescaler and start clock
	TCCR2A = (0x05<<CS20);

	//Make sure global interrupts are enabled
	sei();
}

/*
*	Function Name:	k_signal
*	Purpose:		This function installs a "signal handler" for a function that could
*					potentially lock up. Signals use the timer-counter 1 to count seconds
*					A method counting by seconds could make use of the RTC instead. 
*					The actual delay is approximately 131ms per msecond
*/

void k_signal(void (*function)(void), byte mseconds)
{
	//enable timer OCIE0A output compare interrupt enable
	TIMSK0|=(1<<OCIE0A);

	//Set CTC (clear timer on compare) WGM01:0=2, page 90
	TCCR0A&=~(1<<WGM00);
	TCCR0A|=(1<<WGM01);

	//set count TOP - write count to counter in OCR0A - page 92
	OCR0A=0xFF;

	//enable timer and set clk src internal divide by 1024 prescaler - page 91
	TCCR0A|=0x05;

	//set seconds
	signalMseconds=mseconds;

	//register function pointer to signal handler
	signalFunction=function;

	//Make sure global interrupts are enabled
	sei();
}

/*
*	Function Name:	k_unsignal
*	Purpose:		This function removes the signal handler from k_signal and resets
*					timer-counter 1. 
*/
void k_unsignal()
{
		//Disable interrupt
		TIMSK0&=~(0<<OCIE0A);

		//Stop timer
		TCCR0A&=~(0x07);

		//Unregister function
		signalFunction=NULL;

		//reset counter and seconds
		signalMseconds=0;
		signalCounter=0;
}

/*
*	Function Name:	gather_battery_voltage
*	Purpose:		This function acquires a single analog reading from the battery
*					voltage sensor attached to ADC05 
*/
int gather_battery_voltage()
{
	battery_voltage = get_adc(0x05);
}

/*
*	Function Name:	gather_internal_temp
*	Purpose:		This function acquires a single analog reading from the onboard
*					temperature sensor attached on the AVR butterfly on ADC0
*/
void gather_internal_temp()
{
	internal_temp=get_adc(0x00);
}

/*
*	Function Name:	gather_light_sensor
*	Purpose:		This function acquires a single analog reading from the light sensor
*					connected to ADC06. This is not the LDR built onto the 
*					AVR butterfly.  
*/
void gather_light_sensor()
{
	
	light_sensor=0;

	//LDR is in a voltage divider with V_in provided by the MCU output pin
	//This configures the pin and drives it high, energizing the LDR voltage divider
	LDR_DDR|=0x01<<LDR_PIN;
	LDR_PORT|=0x01<<LDR_PIN;

	//Small delay to let reading stabilize. 
	_delay_ms(10);	

	//Read analog value
	light_sensor+=get_adc(0x06);
	
	//de-energize light sensor
	LDR_PORT&=~(0x01<<LDR_PIN);
}

/*
*	Function Name:	printUptime
*	Purpose:		This function prints the current uptime. 
*/
void printUptime()
{	
	printDecimal(uptime,16);
}

/*
*	Function Name:	TIMER2_OVF_vect
*	Purpose:		This ISR increments the global time counter driven by the RTC
*					once per second. 
*/
ISR(TIMER2_OVF_vect)
{
	//Use Unix Time
	date_time_unix++;
}

/*
*	Function Name:	TIMER0_COMP_vect
*	Purpose:		This ISR is triggered by an overflow interrupt. This occurs
*					periodically, the number of times it occurs indicates whether
*					enough time has passed to send the signal. 
*/
ISR(TIMER0_COMP_vect)
{
	signalCounter++;

	if(signalCounter>=signalMseconds)
	{
		//Call function
		(*signalFunction)();

		//Disable interrupt
		TIMSK0&=~(0<<OCIE0A);

		//Stop timer
		TCCR0A&=~(0x07);

		//Unregister function
		signalFunction=NULL;

		//reset counter and seconds
		signalMseconds=0;
		signalCounter=0;
	}
}

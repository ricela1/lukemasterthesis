/*
*	Filename:	adc.c
*	Date: 		6/16/2013
*	Author:		Luke Rice
*	Description:This file contains the helper driver for the built in analog to digital 
*				converter (ADC) used by the ATMEGA169 and AVR Butterfly. It contains
*				interrupt driven and non-interrupt driven methods of using the 10 bit ADC. 
*
*/

//Includes
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "types.h"

//Buffer for 16 bit analog value.
volatile int adc_value;

//Interrupt Service Routine for Analog to Digital Conversion. 
ISR(ADC_vect)
{
	adc_value+=ADCL;
    adc_value+=(ADCH<<8);
}

/*
*	Function Name: 	k_init_adc(byte channel)
*	Purpose: 		Initialize onboard ADC
*/
void k_init_adc()
{
	//disable JTAG interface. MUST be done at least twice
	//within a short number of cycles (see datasheet)
	MCUCR|=(1<<JTD);
	MCUCR|=(1<<JTD);
	MCUCR|=(1<<JTD);
	MCUCR|=(1<<JTD);

	//enable the ADC
	ADCSRA|=(1<<ADEN);

	//configure PORTF GPIO ports
	DDRF=0x00;
	PORTF=0x00;
	DIDR0=0xFF;
	DIDR0=0xFF;

	//set ADMUX to select ADC4 by default (can be changed later)
	ADMUX|=0x44;

	//set prescaler to div8
	ADCSRA|=0x04;
}

/*
*	Function Name: 	get_adc(byte channel)
*	Purpose: 		Retrieve a single ADC reading on specified channel
*					This function does not use interrupts. 
*/
int get_adc(byte channel)
{
	byte localChannel = channel;
	volatile int partial_result=0x00;
	volatile int result = 0x00;

	//Set ADC channel
	ADMUX&=0xF0;			//Clear channel bits
	localChannel&=0x0F; 	//clear high order bits in case user enters channel > 7
	ADMUX|=localChannel;

	//disable adc interrupts
	ADCSRA&=~(1<<ADIE);

	//Average 8 ADC readings
	for(byte i=0;i<8;i++)
	{
		//start ADC conversion
		ADCSRA|=(1<<ADSC);
	
		//wait for conversion to finish
		while(!(ADCSRA&0x10));
	
		//get Data
		partial_result=ADCL;
	    partial_result+=(ADCH<<8);
		result+=partial_result;
	}
	return (result>>3);
}

/*
*	Function Name: 	get_adc_int(byte channel)
*	Purpose: 		Retrieve a single ADC reading on specified channel using
*					interrupts and noise canceling sleep mode. 
*/
int get_adc_int(byte channel)
{
	//local channel storage
	byte localChannel = channel;

	//Set ADC channel
	ADMUX&=0xF0;			//Clear channel bits
	localChannel&=0x0F; 	//clear high order bits in case user enters channel > 7
	ADMUX|=localChannel;	
	ADCSRA|=(0x01<<ADIE);

	//clear adc buffer value
	adc_value=0;
	
	//Enable ADC noise reduction mode
	set_sleep_mode(SLEEP_MODE_ADC);
	
	//Average 8 ADC readings
	for(byte i=0;i<8;i++)
	{
		//Enable sleep (SE bit)
		cli();
		sleep_enable();	//must be done with interrupts disabled
		sei();

		//Put MCU to sleep
		sleep_cpu(); //sleep_cpu and sleep_mode seem to be equivilant	

		sleep_disable();
	}
	
	//return average
	return adc_value>>3;

}

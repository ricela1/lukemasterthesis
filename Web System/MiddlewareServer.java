/*
*   Filename:   PassThroughServerNew.java
*   Date:       6/16/2013
*   Author:     Luke Rice
*   Description:This file contains the source code for the Middleware part of the Web System. 
*	            This is a multithreaded server that spawns a new client thread whenever
*	            a connection is made to handle issuing the GET request to the Processing 
*	            program. 	
*/

//Imports
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.io.*;
import java.util.*;
import java.text.*;

//Main server thread class
public class MiddlewareServer {

    //Listening port
    private static int port=15004;

    //Main method
    public static void main(String[] args) {
        int i=0; 
        
        //Listen for incoming connection, spawn threads and continue listening
        try{
            //Create server-socket
            ServerSocket listener = new ServerSocket(port);
            Socket server;

            //Thread spawning thread runs indefinitely
            while(true){
                //Accept incoming connection - blocking until a connection is detected
                server = listener.accept();

                //Log request
                InputHandler.logRequest("New connection with client "+i++);
                
                //Create handling thread object
                InputHandler input = new InputHandler(server);

                //Create and start thread. 
                Thread t1 = new Thread(input);
                t1.start();
            }
        } 
        //Catch unexpected error in opening file
        catch (IOException ioe) {
            InputHandler.logRequest("ERROR: IOException on socket listen: " + ioe);
            ioe.printStackTrace();
        }
    }

}

//Thread class for handling individual input
class InputHandler implements Runnable 
{
    //Local sockets for incoming connection and connection to webserver
    private Socket socket,webServer;

    private static String CRLF = "\r\n";

    //Constructor
    InputHandler(Socket server) {
        this.socket=server;
    }

    //Thread method
    public void run () {
        
        //Process request contains bulk of the work
        try {
            processRequest();
        } catch (IOException ioe) {
            logRequest("ERROR: IOException on socket listen: " + ioe);
            ioe.printStackTrace();
        }
    }

    //Bulk of thread-control
    public void processRequest() throws IOException
    {
        //This is the current location and the xml variable is a legacy name, the data is CSV.
        //Generate GET request text
        String messageStart = "GET /ricela1/cosm/processing_script.php?xml=";
        String messageBody = "0.0.uptime,0;ths1_temp,eb;ths1_humid,1bf;ths2_temp,e9;ths2_humid,1a9;hivescale,17;internal_temp,205;DS18B20_EC00000349A49B28,016B;DS18B20_3000000349A7B428,0164;DS18B20_C80000034999A328,016B;DS18B20_E10000034996C428,0168";
        String messageTail = " HTTP/1.1\r\nConnection: close\r\nHost: student.cs.appstate.edu\r\n\r\n";

        //Open stream-reader for reading from socket to device    
        InputStreamReader is = new InputStreamReader(socket.getInputStream());        
        BufferedReader br = new BufferedReader(is);

        //Set timeout on modem->server connection to 10 seconds
        socket.setSoTimeout(30000);
        String requestLine;

        try{
            //Attempt to read from socket - blocking
            requestLine = br.readLine();
            logRequest("input from socket: "+requestLine);
        }
        //Catch timeout or something else
        catch(Exception e)
        {
            logRequest("ERROR: Socket timeout waiting for input from modem");
            socket.close();
            return;
        }
        
        //Detect a request that only has a single character (+CRLF)
        if(requestLine.length() < 3)
        {
            logRequest("ERROR: Request too short :"+requestLine);
            socket.close();
            return;
        }

        //Check to make sure butterfly didn't miss return code from modem and is sending AT commands over and over
        else if(requestLine.charAt(0) == 'A' && requestLine.charAt(1)=='T')
        {
            //Close socket. Butterfly missed the return code saying the socket was opened and is going to sent AT commands until
            //it gets a correct return code (1\r). It is easier to just close the socket and let the butterfly try again in 3 seconds
            //than to attempt to send the return code, listen for data, etc. 
            logRequest("ERROR: AT command detected, closing socket...");
            socket.close();
            return;
        }

        //Good data, begins with the format "X.X., which is usually network.node.<data>
        else if(requestLine.charAt(1)=='.' && requestLine.charAt(3)=='.')
        {
            logRequest("Correct Data Detected ("+requestLine+"), issuing to PHP handler");

            //Close socket BEFORE connection to php script/cosm is made to save power on the device side
            socket.close();

            //Create client socket and send to webserver
            try{
                //Socket creation housekeeping
                InetAddress here = InetAddress.getLocalHost();
                String host = here.getHostName(); //Use localhost
                
                //This actually makes the connection as well
                webServer = new Socket(host,80);

                //Create input and output streams for communicating with webserver
                DataOutputStream ds = new DataOutputStream(webServer.getOutputStream());
                BufferedReader fromWebServer = new BufferedReader(new InputStreamReader(webServer.getInputStream()));

                //Write to webserver
                ds.writeBytes(messageStart);
                ds.writeBytes(requestLine);
                ds.writeBytes(messageTail);

                try{
                    //Set timeout for socket to 30 seconds
                    webServer.setSoTimeout(30000);

                    //Read from webserver input stream until nothing else is sent. 
                    while(true)
                    {
                        String reply = fromWebServer.readLine();
                        if(reply!=null)
                            logRequest(reply);
                        else
                            break;
                    }

                }
                //Detect timeout from setSoTimeout() above
                catch(Exception e)
                {
                    logRequest("ERROR: Timeout waiting for PHP response reached, closing socket");
                    webServer.close();
                }
            }

            //Catch unexpected exception
            catch(Exception e)
            {
                logRequest(e.getMessage());
                webServer.close();
            }
        }

        //Unknown data that is not any of the above errors
        else
        {
            logRequest("ERROR: Unknown data "+requestLine+" - closing socket");
            socket.close(); 
        }
    }

    //Method for logging errors and new connections
    public static void logRequest(String message)
    {
        String filename = "cosm_server.log";
        try{
            //Create file or append if exists
            FileWriter fstream  = new FileWriter(filename,true);
            
            //Create date/timestamp
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd hh:mm:ss a zzz");
            String timestamp = ft.format(dNow);
            
            //Write to file
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(timestamp+" "+message+"\n");
            System.out.println(timestamp+" "+message);
            
            //Close output stream and file
            out.close();
            fstream.close();
        }
        catch(Exception e)
        {   
        }

    }
}

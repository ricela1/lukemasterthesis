<!--
/*
*   Filename:   data_handler.php
*   Date:       6/16/2013
*   Author:     Luke Rice
*   Description:This file contains the serverside portion of the AJAX used in data_access.php to graph and 
*               download sensor data. 
*/
-->

<?php

//Database Connections. Username and password removed. 
$mysqli = new mysqli("localhost","USERNAME","PASSWORD","USERNAME");

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//Post Variables
$startDate=$_POST['startDate']." ".$_POST['startTime'];
$endDate=$_POST['endDate']." ".$_POST['endTime'];
$sensors=$_POST['sensors'];
$avgFreq = $_POST['avgFreq'];
$sdLevel = $_POST['sdLevel'];

//Declare main array that will get converted to JSON object later
$sensor_data=array(
        'dateArray'=>array(),
        'sensors'=>array()
        );

//Retrieve the timestamps over the specified interval. Each sensor will have the 
//exact same timestamps
$get_timestamps ="SELECT DISTINCT datetime FROM `sensor_data` WHERE datetime between '$startDate' and '$endDate'";
$date_results = $mysqli->query($get_timestamps);

//Process timestamps and insert into sensor_data array
if($mysqli->affected_rows)
{
    //Initialize averaging
    $avg_count = 1;

    //Process each record
    while($row=$date_results->fetch_assoc())
    {
        //If no averaging is done, this runs every loop iteration
        //If averaging is done, only ever N timestamps is stored. 
        if($avg_count==$avgFreq)
        {
            //Store timestamp
            array_push($sensor_data['dateArray'],$row['datetime']);
            $avg_count=1;
        }
        else
            $avg_count++;
    }
}

//Retrieve each sensors data
$sensor_count=0;
foreach($sensors as $sensor)
{
    //Retrieve data for individual sensor. 
    $get_data="SELECT UNIX_TIMESTAMP(datetime) AS datetime,value FROM `sensor_data` WHERE datetime between '$startDate' and '$endDate' and sensor_name = '$sensor'";
    $results = $mysqli->query($get_data);

    //Ensure there was at least 1 result
    if($mysqli->affected_rows)
    {
        //Declare results array. This array is formatted like a Highcharts series so it can be directly plotted
        $data_array =  array(
                'data'=>array(),
                'name'=>"$sensor",
                'type'=>'line',
                'yAxis'=>$sensor_count,
                'tooltip'=>array('valueSuffix'=>getUnit($sensor)),  //Get units 
                'min'=>10000,
                'max'=>0);

        //Process actual results
        $avg_count=1;
        $valueSum=0;
        while($row=$results->fetch_assoc())
        {

            //Perform averaging (if average>1)
            $valueSum+=convertData($sensor,$row['value']);

            //If avgFreq  = 1, every datapoint is stored. 
            if($avg_count==$avgFreq){
                
                //Store X,Y data. Unix time is multiplied by 1000 to covert to UTC millisecond time
                //This is to deal with the way Highcharts deals with timestamps
                array_push($data_array['data'],array($row['datetime']*1000,($valueSum*1)/$avgFreq));

                //Find max,min, etc. 
                if($row['value']>$data_array['max'])
                    $data_array['max']=$row['value'];
                if($row['value']<$data_array['min'])
                    $data_array['min']=$row['value'];
                $valueSum=0;
                $avg_count=1;
            }
            else
                $avg_count++;
        }
        //Push array of data for 1 sensor onto return array
        array_push($sensor_data['sensors'],$data_array);

        //Increment sensor counter
        $sensor_count++;
    }
}

//Echo for plotting graph. Content-type ensures that data_access.php can access the results
//as a native javascript array object. 
if(!isset($_POST['download'])){
    header("Content-Type: application/json; charset=UTF-8");    //Important
    echo json_encode((object)$sensor_data);
}

//Echo for downloading excel file. 
else
{
    //Declare array that stores CSV data
    $csvStructure = array(
            'columns'=>array(),
            'data'=>array());

    //Push column headers
    array_push($csvStructure['columns'],"datetime");

    //Push sensor data onto CSV array
    foreach($sensor_data['sensors'] as $individual_sensor)
    {
        array_push($csvStructure['columns'],$individual_sensor['name']);
    }

    //Build CSV array 1 row at a time
    for($i=0;$i<count($sensor_data['sensors'][0]['data']);$i++)
    {
        $row = array();

        //Push date first
        array_push($row,str_replace('"',"",$sensor_data['dateArray'][$i]));
        
        //push 1 reading from each sensor across row
        foreach($sensor_data['sensors'] as $individual_sensor)
        {
            array_push($row,$individual_sensor['data'][$i][1]);
        }

        //Push entire array of data onto CSV array
        array_push($csvStructure['data'],$row);
    }
    
    //Create CSV file    
    $filename = "testFile.csv";
    $output = fopen($filename,'w') or die("Can't open testFile.csv");

    //Output data to CSV file
    fputcsv($output, $csvStructure['columns'],"\t");
    foreach($csvStructure['data'] as $dataArray) {
        fputcsv($output, $dataArray,"\t");
    }
    //Close CSV file
    fclose($output) or die("Can't close $filename");

    //Echo anchor tag to serverside file that can be clicked and downloaded. Should not open within browser
    header("Content-Type: text/html charset=UTF-8"); 
    $echo_array = array('value'=>'<a href=http://student.cs.appstate.edu/ricela1/cosm/'.$filename.'>Download Data</a>');
    echo $echo_array['value']; 
}

//This function converts raw sensor readings into united values
function convertData($sensorID,$data)
{
    $data_value=$data;

    //Check by sensor_id (from database)
    switch($sensorID)
    {
        case "ths1_temp":
            case "ths2_temp":
            case "ths1_humid":
            case "ths2_humid":
            case "outdoor_temp":
            case "outdoor_humid":

            if($data_value>32768){ //Negative temperature - should never happen for humidity
                $data_value=(~$data_value)+1;    //takes 2's comp
                $data_value=0-$data_value;      //get negative value
            }

        //Perform scaling
        $data_value=$data_value/10.0;
        break;

        case "hivescale":
            //"negative" weight value returned because the weight is less than maxweight/2
            if($data>800)
            {
                $weight = ($data-0x8f8)*0.089; //additional f8 accounts for empty weight of scale is 0x8f8
            }
            else
            {
                $weight = ($data+0x708)*0.089; //0x708 accounts for f8 weight of empty scale
            }
        $data_value=$weight;
        break;

        case "internal_temp":
            $data_value = $data;
        $data_value = (4250/(log($data_value/(1024-$data_value),2.71828182846)+(4240.0/298.0)))-273.0;
        break;

        case (substr($sensorID,0,7)=="DS18B20"?true:false):
            $data_value=$data;

        //Check for negative and retrieve negative value
        if($data_value>32768)
        {
            $data_value=(~$data_value)+1;    //takes 2's comp
            $data_value=0-$data_value;      //get negative value
        }

        //perform scaling
        $data_value = $data_value*.0625;
        break;

        case "battery_voltage":
            $scaled_voltage = ($data/1024)*5.0;     //compute % of full scale voltage
        $data_value = $scaled_voltage/0.33637152;   //use voltage divider calculation (Vo=Vi(R2/(R1+R2))) Checked battery voltage before one of the last tests, at ~13.05V the output was 899 (on scale of 1024) or about 4.39V. Calculated the ratio of R2/R1+R2 instead of using actual resistor values, which are only accurate to +-10%
        break;
    }

    return $data_value;
}

//Determine what units to use for sensor for output on tooltips and 
//Y axes in highcharts
function getUnit($sensorID)
{
    $unit="";

    switch($sensorID)
    {
        //All temperature sensors use Celsius
        case "ths1_temp":
            case "ths2_temp":
            case "outdoor_temp":
            case "internal_temp":
            case (substr($sensorID,0,7)=="DS18B20"?true:false):
            $unit="C";
        break;

        //Relative humidity is measured in percent
        case "outdoor_humid":
            case "ths1_humid":
            case "ths2_humid":
            $unit="%";
        break;

        //Scale uses pounds for weight
        case "hivescale":
            $unit="lb";
        break;

        //Battery voltage is in measurements of Volts
        case "battery_voltage":
            $unit="V";
        break;
    }

    return $unit;
}

?>

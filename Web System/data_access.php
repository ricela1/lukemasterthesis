<!--
/*
*   Filename:   data_access.php
*   Date:       6/16/2013
*   Author:     Luke Rice
*   Description:This file contains the source code for visualizing sensor data and downloading in CSV format. 
*/
-->

<?php 

//Database Connections. Password and username removed. 
$mysqli = new mysqli("localhost","USERNAME","PASSWORD","USERNAME");

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

//Retrieve the list of sensors in the database
$results = $mysqli->query("SELECT DISTINCT sensor_name FROM `sensor_data` WHERE 1");
$sensor_names=array();

if($mysqli->affected_rows)
{
    while($row=$results->fetch_assoc())
    {
        array_push($sensor_names,$row['sensor_name']);
    }
}
?>

<!doctype html> 
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Hive Data Downloader</title>
  
  <!--CDN (Content Delivery Network) info for javascript frameworks used -->
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<!-- Graph drawing javascript - this is called when the user clicks display -->
<script type="text/javascript">
function makeGraph(){
    var chart;
    $(document).ready(function() {
    
        //Define the options, multi-dimentional array. 
        var options = {
    
            chart: {
                renderTo: 'container'   //Set HTML element that will contain the graph
            },
    
            title: {
                text: 'Sensor Data'
            },
    
            subtitle: {
                text: ' '
            },
    
            xAxis: {
                type: 'datetime',
            },
             
            yAxis: [],                  //Declare Yaxes are an array (for multiple Y axes)
    
            legend: {
                align: 'left',
                verticalAlign: 'top',
                y: 20,
                floating: true,
                borderWidth: 0
            },
    
            tooltip: {
                shared: true,           //Tooltip when mousing over points. 
                crosshairs: true
            },
    
            //How to draw individual points
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: this.pageX,
                                        y: this.pageY
                                    },
                                    headingText: this.series.name,
                                    maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) +':<br/> '+
                                        this.y +' visits',
                                    width: 200
                                });
                            }
                        }
                    },
                    marker: {
                        lineWidth: 1
                    }
                }
            },
    
            series: []                  //Set up for multiple series
        };
    
    
    //Start AJAX code

    //Declare HTTP variables
    var startDate = $('#start_date').val();
    var endDate =   $('#end_date').val();
    var startTime = $('#start_hour').val()+":"+$('#start_minute').val()+":00";
    var endTime =   $('#end_hour').val()+":"+$('#end_minute').val()+":00";
    
    //Determine which sensors to pull data for
    var sensors =[];
    $('input:checkbox:checked.sensor_name').each(function(){ sensors.push(this.value); });
    
    //Determine what level of averaging and standard deviation were selected (standard deviation isn't implemented on the serverside) 
    var avgFreq =   $('#avg_freq').val()
    var sdLevel =   $('#sd_level').val();

    //Make sure results field is hidden.
    $('#results').hide();

    //Actual AJAX call
    $.ajax({
        url: 'http://student.cs.appstate.edu/ricela1/cosm/data_handler.php',
        type: 'POST',
        data: {
            'startDate': startDate,
            'endDate': endDate,
            'startTime': startTime,
            'endTime': endTime,
            'sensors[]':sensors,
            'avgFreq': avgFreq,
            'sdLevel': sdLevel
        },

        
        //Process response. Response is going to be a big JSON object that basically mimics the structure of
        //HIGHCHARTS series's. 
        success: function(data) {

               //Loop through each series
               $.each(data.sensors,function(sensor, value){
                    
                    //Alternate which side of the graph the Y axis is drawn on. 
                    if(value.yAxis%2==0)
                    {
                        //Push new Y Axis on left
                        options.yAxis.push({title: {text: value.name},labels: {formatter: function(){ return this.value.toFixed(2)+' '+value.tooltip.valueSuffix;}}});                    
                    }
                    else
                    {
                        //Push new Y axis on the right
                        options.yAxis.push({title: {text: value.name},opposite: true, labels: {/*align: 'left',x:5,y: 16,*/formatter: function(){return this.value.toFixed(2)+' '+value.tooltip.valueSuffix}}});                    
                    }

                    //Push values for individual series
                    options.series.push(value);
               });

            //Create new chart using options. Options array now contains all series and X and Y values for each.
            chart = new Highcharts.Chart(options);
        }
    });
    });
}
</script>

<!-- Anonymous function for displaying date-picker from JQuery -->
<script>
  $(function() {
    $( "#start_date,#end_date" ).datepicker({dateFormat: "yy-mm-dd" });
    $("#container").show();
  });

// Function that installs actionlisteners for submit and download buttons
$(function() {
    $('#submit').click(function() {
        makeGraph();
    });
    $('#download').click(function() {
        doSearch();
    });
}); 
 
//Function called when user clicks download (to download data in CSV)
function doSearch() {
      
    //Ajax HTTP variables
    var startDate = $('#start_date').val();
    var endDate =   $('#end_date').val();
    var startTime = $('#start_hour').val()+":"+$('#start_minute').val()+":00";
    var endTime =   $('#end_hour').val()+":"+$('#end_minute').val()+":00";
    var avgFreq =   $('#avg_freq').val()
    var sdLevel =   $('#sd_level').val();
    
    //Determine which sensors were requested
    var sensors =[];
    $('input:checkbox:checked.sensor_name').each(function(){ sensors.push(this.value); });
    
    $('#results').hide();
    
    //Make AJAX request
    $.ajax({
        url: 'http://student.cs.appstate.edu/ricela1/cosm/data_handler.php',
        type: 'POST',
        data: {
            'startDate': startDate,
            'endDate': endDate,
            'startTime': startTime,
            'endTime': endTime,
            'sensors[]':sensors,
            'avgFreq': avgFreq,
            'sdLevel': sdLevel,
            'download':"true"
        },
        
        //Attach anchor tag to .csv file on the server to be clicked
        success: function(data) {
                $('#download_hook').empty();
                $('#download_hook').html(data);
        }
    });
}
  
  </script>
</head>
<body>
 
<!-- Form HTML and PHP --> 
<p>Start Date: <input type="text" name="start_date" id="start_date" value="<?php echo "2013-04-06"; ?>"/>
    Start Hour <select name="start_hour" id="start_hour"><?php foreach(range(0,23) as $time){echo'<option>';echo $time; echo'</option>';}?></select>
    Start Minute <select name="start_minute" id="start_minute"><?php foreach(range(0,59) as $time){echo'<option>';echo $time; echo'</option>';}?></select>
        
</p>

<p>End Date: <input type="text" id="end_date" name="end_date" value="<?php echo "2013-04-06"; ?>" />
    End Hour <select name="end_hour" id="end_hour"><?php foreach(range(0,23) as $time){echo'<option>';echo $time; echo'</option>';}?></select>
    End Minute <select name="end_minute" id="end_minute"><?php foreach(range(0,59) as $time){echo'<option>';echo $time; echo'</option>';}?></select>
</p>

<fieldset>
<legend>Sensors</legend>
<?php 
foreach($sensor_names as $value){echo "<input type=\"checkbox\" class=\"sensor_name\" value=\"$value\">$value</input><br />";}
?>
</fieldset>

<p>Average frequency <select name="avg_freq" id="avg_freq"><?php foreach(range(1,300) as $samples){echo'<option>';echo $samples; echo'</option>';}?></select> 
    (0 for selecting every sample) </p>

<p>Standard Deviations? <select name="sd_level" id="sd_level"><?php foreach(range(0,60) as $samples){echo'<option>';echo $samples; echo'</option>';}?></select> 
    (0 for no SD culling) </p>

<button id="submit">Graph Data</button>
<button id="download">Download Excel</button>

<!-- Empty result tags -->
<p id="results"></p>
<p id="good_data"></p>
<span id="download_hook"> </span>

<!-- Highcharts source code -->
<script src="http://student.cs.appstate.edu/ricela1/highcharts/js/highcharts.js"></script>
<script src="http://student.cs.appstate.edu/ricela1/highcharts/js/modules/exporting.js"></script>

<!-- Additional files from CDN for the Highslide popup effect -->
<script type="text/javascript" src="http://www.highcharts.com/highslide/highslide-full.min.js"></script>
<script type="text/javascript" src="http://www.highcharts.com/highslide/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="http://www.highcharts.com/highslide/highslide.css" />

<!-- Container for Highcharts chart -->
<div id="container" style="min-width: 95%; height: 600px; margin: 0 auto"></div>

</body>
</html>
